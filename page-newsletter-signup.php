<html>
<head>
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1"
    />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>
        Carolina Arms Group : Sign Up to Stay in Touch
    </title>

    <script
        type="text/javascript"
        src="https://bam.nr-data.net/1/5ab79a9e36?a=1784721&amp;v=1167.2a4546b&amp;to=Y1MHYRBUCBJZBRJfW1oZL2YyGhIIVAMVelVNWRBBTVkHGFcTEhheR0Y%3D&amp;rst=2451&amp;ref=https://visitor.r20.constantcontact.com/manage/optin&amp;ap=200&amp;be=2103&amp;fe=2331&amp;dc=2197&amp;af=err,xhr&amp;perf=%7B%22timing%22:%7B%22of%22:1586777867916,%22n%22:0,%22f%22:804,%22dn%22:804,%22dne%22:804,%22c%22:804,%22ce%22:804,%22rq%22:805,%22rp%22:1098,%22rpe%22:1440,%22dl%22:1126,%22di%22:2187,%22ds%22:2187,%22de%22:2197,%22dc%22:2330,%22l%22:2330,%22le%22:2331%7D,%22navigation%22:%7B%7D%7D&amp;fp=2300&amp;fcp=2300&amp;jsonp=NREUM.setToken"
    ></script>
    <script src="https://js-agent.newrelic.com/nr-1167.min.js"></script>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>

    <link
        href="https://static.ctctcdn.com/ui/vendor/jquery-ui-1.8.17/jquery.ui.base.css?version=20161010091324"
        media="all"
        rel="stylesheet"
        type="text/css"
    />

    <script>
        jQuery.noConflict();
    </script>

    <link
        href="/core/css/base.css?version=20161010091324"
        rel="stylesheet"
        type="text/css"
    />
    <link
        href="/core/css/visitor.css?version=20161010091324"
        rel="stylesheet"
        type="text/css"
    />

    <link
        href="https://www.constantcontact.com/favicon.ico?version=20161010091324"
        rel="icon"
    />

    <script type="text/javascript">
        (window.NREUM || (NREUM = {})).loader_config = {
            xpid: "UwYAV1BACQQIUFZQBg==",
            licenseKey: "5ab79a9e36",
            applicationID: "1784721",
        };
        window.NREUM || (NREUM = {}),
            (__nr_require = (function (t, e, n) {
                function r(n) {
                    if (!e[n]) {
                        var o = (e[n] = { exports: {} });
                        t[n][0].call(
                            o.exports,
                            function (e) {
                                var o = t[n][1][e];
                                return r(o || e);
                            },
                            o,
                            o.exports
                        );
                    }
                    return e[n].exports;
                }
                if ("function" == typeof __nr_require) return __nr_require;
                for (var o = 0; o < n.length; o++) r(n[o]);
                return r;
            })(
                {
                    1: [
                        function (t, e, n) {
                            function r(t) {
                                try {
                                    s.console && console.log(t);
                                } catch (e) {}
                            }
                            var o,
                                i = t("ee"),
                                a = t(18),
                                s = {};
                            try {
                                (o = localStorage.getItem("__nr_flags").split(",")),
                                console &&
                                "function" == typeof console.log &&
                                ((s.console = !0),
                                o.indexOf("dev") !== -1 && (s.dev = !0),
                                o.indexOf("nr_dev") !== -1 && (s.nrDev = !0));
                            } catch (c) {}
                            s.nrDev &&
                            i.on("internal-error", function (t) {
                                r(t.stack);
                            }),
                            s.dev &&
                            i.on("fn-err", function (t, e, n) {
                                r(n.stack);
                            }),
                            s.dev &&
                            (r("NR AGENT IN DEVELOPMENT MODE"),
                                r(
                                    "flags: " +
                                    a(s, function (t, e) {
                                        return t;
                                    }).join(", ")
                                ));
                        },
                        {},
                    ],
                    2: [
                        function (t, e, n) {
                            function r(t, e, n, r, s) {
                                try {
                                    p ? (p -= 1) : o(s || new UncaughtException(t, e, n), !0);
                                } catch (f) {
                                    try {
                                        i("ierr", [f, c.now(), !0]);
                                    } catch (d) {}
                                }
                                return "function" == typeof u && u.apply(this, a(arguments));
                            }
                            function UncaughtException(t, e, n) {
                                (this.message =
                                    t || "Uncaught error with no additional information"),
                                    (this.sourceURL = e),
                                    (this.line = n);
                            }
                            function o(t, e) {
                                var n = e ? null : c.now();
                                i("err", [t, n]);
                            }
                            var i = t("handle"),
                                a = t(19),
                                s = t("ee"),
                                c = t("loader"),
                                f = t("gos"),
                                u = window.onerror,
                                d = !1,
                                l = "nr@seenError",
                                p = 0;
                            (c.features.err = !0), t(1), (window.onerror = r);
                            try {
                                throw new Error();
                            } catch (h) {
                                "stack" in h &&
                                (t(6),
                                    t(5),
                                "addEventListener" in window && t(3),
                                c.xhrWrappable && t(7),
                                    (d = !0));
                            }
                            s.on("fn-start", function (t, e, n) {
                                d && (p += 1);
                            }),
                                s.on("fn-err", function (t, e, n) {
                                    d &&
                                    !n[l] &&
                                    (f(n, l, function () {
                                        return !0;
                                    }),
                                        (this.thrown = !0),
                                        o(n));
                                }),
                                s.on("fn-end", function () {
                                    d && !this.thrown && p > 0 && (p -= 1);
                                }),
                                s.on("internal-error", function (t) {
                                    i("ierr", [t, c.now(), !0]);
                                });
                        },
                        {},
                    ],
                    3: [
                        function (t, e, n) {
                            function r(t) {
                                for (var e = t; e && !e.hasOwnProperty(u); )
                                    e = Object.getPrototypeOf(e);
                                e && o(e);
                            }
                            function o(t) {
                                s.inPlace(t, [u, d], "-", i);
                            }
                            function i(t, e) {
                                return t[1];
                            }
                            var a = t("ee").get("events"),
                                s = t("wrap-function")(a, !0),
                                c = t("gos"),
                                f = XMLHttpRequest,
                                u = "addEventListener",
                                d = "removeEventListener";
                            (e.exports = a),
                                "getPrototypeOf" in Object
                                    ? (r(document), r(window), r(f.prototype))
                                    : f.prototype.hasOwnProperty(u) &&
                                    (o(window), o(f.prototype)),
                                a.on(u + "-start", function (t, e) {
                                    var n = t[1],
                                        r = c(n, "nr@wrapped", function () {
                                            function t() {
                                                if ("function" == typeof n.handleEvent)
                                                    return n.handleEvent.apply(n, arguments);
                                            }
                                            var e = { object: t, function: n }[typeof n];
                                            return e ? s(e, "fn-", null, e.name || "anonymous") : n;
                                        });
                                    this.wrapped = t[1] = r;
                                }),
                                a.on(d + "-start", function (t) {
                                    t[1] = this.wrapped || t[1];
                                });
                        },
                        {},
                    ],
                    4: [
                        function (t, e, n) {
                            function r(t, e, n) {
                                var r = t[e];
                                "function" == typeof r &&
                                (t[e] = function () {
                                    var t = i(arguments),
                                        e = {};
                                    o.emit(n + "before-start", [t], e);
                                    var a;
                                    e[m] && e[m].dt && (a = e[m].dt);
                                    var s = r.apply(this, t);
                                    return (
                                        o.emit(n + "start", [t, a], s),
                                            s.then(
                                                function (t) {
                                                    return o.emit(n + "end", [null, t], s), t;
                                                },
                                                function (t) {
                                                    throw (o.emit(n + "end", [t], s), t);
                                                }
                                            )
                                    );
                                });
                            }
                            var o = t("ee").get("fetch"),
                                i = t(19),
                                a = t(18);
                            e.exports = o;
                            var s = window,
                                c = "fetch-",
                                f = c + "body-",
                                u = ["arrayBuffer", "blob", "json", "text", "formData"],
                                d = s.Request,
                                l = s.Response,
                                p = s.fetch,
                                h = "prototype",
                                m = "nr@context";
                            d &&
                            l &&
                            p &&
                            (a(u, function (t, e) {
                                r(d[h], e, f), r(l[h], e, f);
                            }),
                                r(s, "fetch", c),
                                o.on(c + "end", function (t, e) {
                                    var n = this;
                                    if (e) {
                                        var r = e.headers.get("content-length");
                                        null !== r && (n.rxSize = r),
                                            o.emit(c + "done", [null, e], n);
                                    } else o.emit(c + "done", [t], n);
                                }));
                        },
                        {},
                    ],
                    5: [
                        function (t, e, n) {
                            var r = t("ee").get("raf"),
                                o = t("wrap-function")(r),
                                i = "equestAnimationFrame";
                            (e.exports = r),
                                o.inPlace(
                                    window,
                                    ["r" + i, "mozR" + i, "webkitR" + i, "msR" + i],
                                    "raf-"
                                ),
                                r.on("raf-start", function (t) {
                                    t[0] = o(t[0], "fn-");
                                });
                        },
                        {},
                    ],
                    6: [
                        function (t, e, n) {
                            function r(t, e, n) {
                                t[0] = a(t[0], "fn-", null, n);
                            }
                            function o(t, e, n) {
                                (this.method = n),
                                    (this.timerDuration = isNaN(t[1]) ? 0 : +t[1]),
                                    (t[0] = a(t[0], "fn-", this, n));
                            }
                            var i = t("ee").get("timer"),
                                a = t("wrap-function")(i),
                                s = "setTimeout",
                                c = "setInterval",
                                f = "clearTimeout",
                                u = "-start",
                                d = "-";
                            (e.exports = i),
                                a.inPlace(window, [s, "setImmediate"], s + d),
                                a.inPlace(window, [c], c + d),
                                a.inPlace(window, [f, "clearImmediate"], f + d),
                                i.on(c + u, r),
                                i.on(s + u, o);
                        },
                        {},
                    ],
                    7: [
                        function (t, e, n) {
                            function r(t, e) {
                                d.inPlace(e, ["onreadystatechange"], "fn-", s);
                            }
                            function o() {
                                var t = this,
                                    e = u.context(t);
                                t.readyState > 3 &&
                                !e.resolved &&
                                ((e.resolved = !0), u.emit("xhr-resolved", [], t)),
                                    d.inPlace(t, w, "fn-", s);
                            }
                            function i(t) {
                                y.push(t),
                                h && (b ? b.then(a) : v ? v(a) : ((E = -E), (O.data = E)));
                            }
                            function a() {
                                for (var t = 0; t < y.length; t++) r([], y[t]);
                                y.length && (y = []);
                            }
                            function s(t, e) {
                                return e;
                            }
                            function c(t, e) {
                                for (var n in t) e[n] = t[n];
                                return e;
                            }
                            t(3);
                            var f = t("ee"),
                                u = f.get("xhr"),
                                d = t("wrap-function")(u),
                                l = NREUM.o,
                                p = l.XHR,
                                h = l.MO,
                                m = l.PR,
                                v = l.SI,
                                g = "readystatechange",
                                w = [
                                    "onload",
                                    "onerror",
                                    "onabort",
                                    "onloadstart",
                                    "onloadend",
                                    "onprogress",
                                    "ontimeout",
                                ],
                                y = [];
                            e.exports = u;
                            var x = (window.XMLHttpRequest = function (t) {
                                var e = new p(t);
                                try {
                                    u.emit("new-xhr", [e], e), e.addEventListener(g, o, !1);
                                } catch (n) {
                                    try {
                                        u.emit("internal-error", [n]);
                                    } catch (r) {}
                                }
                                return e;
                            });
                            if (
                                (c(p, x),
                                    (x.prototype = p.prototype),
                                    d.inPlace(x.prototype, ["open", "send"], "-xhr-", s),
                                    u.on("send-xhr-start", function (t, e) {
                                        r(t, e), i(e);
                                    }),
                                    u.on("open-xhr-start", r),
                                    h)
                            ) {
                                var b = m && m.resolve();
                                if (!v && !m) {
                                    var E = 1,
                                        O = document.createTextNode(E);
                                    new h(a).observe(O, { characterData: !0 });
                                }
                            } else
                                f.on("fn-end", function (t) {
                                    (t[0] && t[0].type === g) || a();
                                });
                        },
                        {},
                    ],
                    8: [
                        function (t, e, n) {
                            function r(t) {
                                if (!i(t)) return null;
                                var e = window.NREUM;
                                if (!e.loader_config) return null;
                                var n = (e.loader_config.accountID || "").toString() || null,
                                    r = (e.loader_config.agentID || "").toString() || null,
                                    s = (e.loader_config.trustKey || "").toString() || null;
                                if (!n || !r) return null;
                                var c = a.generateCatId(),
                                    f = a.generateCatId(),
                                    u = Date.now(),
                                    d = o(c, f, u, n, r, s);
                                return { header: d, guid: c, traceId: f, timestamp: u };
                            }
                            function o(t, e, n, r, o, i) {
                                var a = "btoa" in window && "function" == typeof window.btoa;
                                if (!a) return null;
                                var s = {
                                    v: [0, 1],
                                    d: { ty: "Browser", ac: r, ap: o, id: t, tr: e, ti: n },
                                };
                                return i && r !== i && (s.d.tk = i), btoa(JSON.stringify(s));
                            }
                            function i(t) {
                                var e = !1,
                                    n = !1,
                                    r = {};
                                if (
                                    ("init" in NREUM &&
                                    "distributed_tracing" in NREUM.init &&
                                    ((r = NREUM.init.distributed_tracing), (n = !!r.enabled)),
                                        n)
                                )
                                    if (t.sameOrigin) e = !0;
                                    else if (r.allowed_origins instanceof Array)
                                        for (var o = 0; o < r.allowed_origins.length; o++) {
                                            var i = s(r.allowed_origins[o]);
                                            if (
                                                t.hostname === i.hostname &&
                                                t.protocol === i.protocol &&
                                                t.port === i.port
                                            ) {
                                                e = !0;
                                                break;
                                            }
                                        }
                                return n && e;
                            }
                            var a = t(16),
                                s = t(10);
                            e.exports = { generateTracePayload: r, shouldGenerateTrace: i };
                        },
                        {},
                    ],
                    9: [
                        function (t, e, n) {
                            function r(t) {
                                var e = this.params,
                                    n = this.metrics;
                                if (!this.ended) {
                                    this.ended = !0;
                                    for (var r = 0; r < l; r++)
                                        t.removeEventListener(d[r], this.listener, !1);
                                    e.aborted ||
                                    ((n.duration = a.now() - this.startTime),
                                        this.loadCaptureCalled || 4 !== t.readyState
                                            ? null == e.status && (e.status = 0)
                                            : i(this, t),
                                        (n.cbTime = this.cbTime),
                                        u.emit("xhr-done", [t], t),
                                        s("xhr", [e, n, this.startTime]));
                                }
                            }
                            function o(t, e) {
                                var n = c(e),
                                    r = t.params;
                                (r.host = n.hostname + ":" + n.port),
                                    (r.pathname = n.pathname),
                                    (t.parsedOrigin = c(e)),
                                    (t.sameOrigin = t.parsedOrigin.sameOrigin);
                            }
                            function i(t, e) {
                                t.params.status = e.status;
                                var n = v(e, t.lastSize);
                                if ((n && (t.metrics.rxSize = n), t.sameOrigin)) {
                                    var r = e.getResponseHeader("X-NewRelic-App-Data");
                                    r && (t.params.cat = r.split(", ").pop());
                                }
                                t.loadCaptureCalled = !0;
                            }
                            var a = t("loader");
                            if (a.xhrWrappable) {
                                var s = t("handle"),
                                    c = t(10),
                                    f = t(8).generateTracePayload,
                                    u = t("ee"),
                                    d = ["load", "error", "abort", "timeout"],
                                    l = d.length,
                                    p = t("id"),
                                    h = t(14),
                                    m = t(13),
                                    v = t(11),
                                    g = window.XMLHttpRequest;
                                (a.features.xhr = !0),
                                    t(7),
                                    t(4),
                                    u.on("new-xhr", function (t) {
                                        var e = this;
                                        (e.totalCbs = 0),
                                            (e.called = 0),
                                            (e.cbTime = 0),
                                            (e.end = r),
                                            (e.ended = !1),
                                            (e.xhrGuids = {}),
                                            (e.lastSize = null),
                                            (e.loadCaptureCalled = !1),
                                            t.addEventListener(
                                                "load",
                                                function (n) {
                                                    i(e, t);
                                                },
                                                !1
                                            ),
                                        (h && (h > 34 || h < 10)) ||
                                        window.opera ||
                                        t.addEventListener(
                                            "progress",
                                            function (t) {
                                                e.lastSize = t.loaded;
                                            },
                                            !1
                                        );
                                    }),
                                    u.on("open-xhr-start", function (t) {
                                        (this.params = { method: t[0] }),
                                            o(this, t[1]),
                                            (this.metrics = {});
                                    }),
                                    u.on("open-xhr-end", function (t, e) {
                                        "loader_config" in NREUM &&
                                        "xpid" in NREUM.loader_config &&
                                        this.sameOrigin &&
                                        e.setRequestHeader(
                                            "X-NewRelic-ID",
                                            NREUM.loader_config.xpid
                                        );
                                        var n = f(this.parsedOrigin);
                                        n &&
                                        n.header &&
                                        (e.setRequestHeader("newrelic", n.header),
                                            (this.dt = n));
                                    }),
                                    u.on("send-xhr-start", function (t, e) {
                                        var n = this.metrics,
                                            r = t[0],
                                            o = this;
                                        if (n && r) {
                                            var i = m(r);
                                            i && (n.txSize = i);
                                        }
                                        (this.startTime = a.now()),
                                            (this.listener = function (t) {
                                                try {
                                                    "abort" !== t.type ||
                                                    o.loadCaptureCalled ||
                                                    (o.params.aborted = !0),
                                                    ("load" !== t.type ||
                                                        (o.called === o.totalCbs &&
                                                            (o.onloadCalled ||
                                                                "function" != typeof e.onload))) &&
                                                    o.end(e);
                                                } catch (n) {
                                                    try {
                                                        u.emit("internal-error", [n]);
                                                    } catch (r) {}
                                                }
                                            });
                                        for (var s = 0; s < l; s++)
                                            e.addEventListener(d[s], this.listener, !1);
                                    }),
                                    u.on("xhr-cb-time", function (t, e, n) {
                                        (this.cbTime += t),
                                            e ? (this.onloadCalled = !0) : (this.called += 1),
                                        this.called !== this.totalCbs ||
                                        (!this.onloadCalled &&
                                            "function" == typeof n.onload) ||
                                        this.end(n);
                                    }),
                                    u.on("xhr-load-added", function (t, e) {
                                        var n = "" + p(t) + !!e;
                                        this.xhrGuids &&
                                        !this.xhrGuids[n] &&
                                        ((this.xhrGuids[n] = !0), (this.totalCbs += 1));
                                    }),
                                    u.on("xhr-load-removed", function (t, e) {
                                        var n = "" + p(t) + !!e;
                                        this.xhrGuids &&
                                        this.xhrGuids[n] &&
                                        (delete this.xhrGuids[n], (this.totalCbs -= 1));
                                    }),
                                    u.on("addEventListener-end", function (t, e) {
                                        e instanceof g &&
                                        "load" === t[0] &&
                                        u.emit("xhr-load-added", [t[1], t[2]], e);
                                    }),
                                    u.on("removeEventListener-end", function (t, e) {
                                        e instanceof g &&
                                        "load" === t[0] &&
                                        u.emit("xhr-load-removed", [t[1], t[2]], e);
                                    }),
                                    u.on("fn-start", function (t, e, n) {
                                        e instanceof g &&
                                        ("onload" === n && (this.onload = !0),
                                        ("load" === (t[0] && t[0].type) || this.onload) &&
                                        (this.xhrCbStart = a.now()));
                                    }),
                                    u.on("fn-end", function (t, e) {
                                        this.xhrCbStart &&
                                        u.emit(
                                            "xhr-cb-time",
                                            [a.now() - this.xhrCbStart, this.onload, e],
                                            e
                                        );
                                    }),
                                    u.on("fetch-before-start", function (t) {
                                        var e,
                                            n = t[1] || {};
                                        "string" == typeof t[0]
                                            ? (e = t[0])
                                            : t[0] && t[0].url && (e = t[0].url),
                                        e &&
                                        ((this.parsedOrigin = c(e)),
                                            (this.sameOrigin = this.parsedOrigin.sameOrigin));
                                        var r = f(this.parsedOrigin);
                                        if (r && r.header) {
                                            var o = r.header;
                                            if ("string" == typeof t[0]) {
                                                var i = {};
                                                for (var a in n) i[a] = n[a];
                                                (i.headers = new Headers(n.headers || {})),
                                                    i.headers.set("newrelic", o),
                                                    (this.dt = r),
                                                    t.length > 1 ? (t[1] = i) : t.push(i);
                                            } else
                                                t[0] &&
                                                t[0].headers &&
                                                (t[0].headers.append("newrelic", o), (this.dt = r));
                                        }
                                    });
                            }
                        },
                        {},
                    ],
                    10: [
                        function (t, e, n) {
                            var r = {};
                            e.exports = function (t) {
                                if (t in r) return r[t];
                                var e = document.createElement("a"),
                                    n = window.location,
                                    o = {};
                                (e.href = t), (o.port = e.port);
                                var i = e.href.split("://");
                                !o.port &&
                                i[1] &&
                                (o.port = i[1]
                                    .split("/")[0]
                                    .split("@")
                                    .pop()
                                    .split(":")[1]),
                                (o.port && "0" !== o.port) ||
                                (o.port = "https" === i[0] ? "443" : "80"),
                                    (o.hostname = e.hostname || n.hostname),
                                    (o.pathname = e.pathname),
                                    (o.protocol = i[0]),
                                "/" !== o.pathname.charAt(0) &&
                                (o.pathname = "/" + o.pathname);
                                var a =
                                    !e.protocol ||
                                    ":" === e.protocol ||
                                    e.protocol === n.protocol,
                                    s = e.hostname === document.domain && e.port === n.port;
                                return (
                                    (o.sameOrigin = a && (!e.hostname || s)),
                                    "/" === o.pathname && (r[t] = o),
                                        o
                                );
                            };
                        },
                        {},
                    ],
                    11: [
                        function (t, e, n) {
                            function r(t, e) {
                                var n = t.responseType;
                                return "json" === n && null !== e
                                    ? e
                                    : "arraybuffer" === n || "blob" === n || "json" === n
                                        ? o(t.response)
                                        : "text" === n ||
                                        "document" === n ||
                                        "" === n ||
                                        void 0 === n
                                            ? o(t.responseText)
                                            : void 0;
                            }
                            var o = t(13);
                            e.exports = r;
                        },
                        {},
                    ],
                    12: [
                        function (t, e, n) {
                            function r() {}
                            function o(t, e, n) {
                                return function () {
                                    return (
                                        i(t, [f.now()].concat(s(arguments)), e ? null : this, n),
                                            e ? void 0 : this
                                    );
                                };
                            }
                            var i = t("handle"),
                                a = t(18),
                                s = t(19),
                                c = t("ee").get("tracer"),
                                f = t("loader"),
                                u = NREUM;
                            "undefined" == typeof window.newrelic && (newrelic = u);
                            var d = [
                                    "setPageViewName",
                                    "setCustomAttribute",
                                    "setErrorHandler",
                                    "finished",
                                    "addToTrace",
                                    "inlineHit",
                                    "addRelease",
                                ],
                                l = "api-",
                                p = l + "ixn-";
                            a(d, function (t, e) {
                                u[e] = o(l + e, !0, "api");
                            }),
                                (u.addPageAction = o(l + "addPageAction", !0)),
                                (u.setCurrentRouteName = o(l + "routeName", !0)),
                                (e.exports = newrelic),
                                (u.interaction = function () {
                                    return new r().get();
                                });
                            var h = (r.prototype = {
                                createTracer: function (t, e) {
                                    var n = {},
                                        r = this,
                                        o = "function" == typeof e;
                                    return (
                                        i(p + "tracer", [f.now(), t, n], r),
                                            function () {
                                                if (
                                                    (c.emit(
                                                        (o ? "" : "no-") + "fn-start",
                                                        [f.now(), r, o],
                                                        n
                                                    ),
                                                        o)
                                                )
                                                    try {
                                                        return e.apply(this, arguments);
                                                    } catch (t) {
                                                        throw (
                                                            (c.emit("fn-err", [arguments, this, t], n), t)
                                                        );
                                                    } finally {
                                                        c.emit("fn-end", [f.now()], n);
                                                    }
                                            }
                                    );
                                },
                            });
                            a(
                                "actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(
                                    ","
                                ),
                                function (t, e) {
                                    h[e] = o(p + e);
                                }
                            ),
                                (newrelic.noticeError = function (t, e) {
                                    "string" == typeof t && (t = new Error(t)),
                                        i("err", [t, f.now(), !1, e]);
                                });
                        },
                        {},
                    ],
                    13: [
                        function (t, e, n) {
                            e.exports = function (t) {
                                if ("string" == typeof t && t.length) return t.length;
                                if ("object" == typeof t) {
                                    if (
                                        "undefined" != typeof ArrayBuffer &&
                                        t instanceof ArrayBuffer &&
                                        t.byteLength
                                    )
                                        return t.byteLength;
                                    if (
                                        "undefined" != typeof Blob &&
                                        t instanceof Blob &&
                                        t.size
                                    )
                                        return t.size;
                                    if (
                                        !("undefined" != typeof FormData && t instanceof FormData)
                                    )
                                        try {
                                            return JSON.stringify(t).length;
                                        } catch (e) {
                                            return;
                                        }
                                }
                            };
                        },
                        {},
                    ],
                    14: [
                        function (t, e, n) {
                            var r = 0,
                                o = navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);
                            o && (r = +o[1]), (e.exports = r);
                        },
                        {},
                    ],
                    15: [
                        function (t, e, n) {
                            function r(t, e) {
                                var n = t.getEntries();
                                n.forEach(function (t) {
                                    "first-paint" === t.name
                                        ? c("timing", ["fp", Math.floor(t.startTime)])
                                        : "first-contentful-paint" === t.name &&
                                        c("timing", ["fcp", Math.floor(t.startTime)]);
                                });
                            }
                            function o(t, e) {
                                var n = t.getEntries();
                                n.length > 0 && c("lcp", [n[n.length - 1]]);
                            }
                            function i(t) {
                                if (t instanceof u && !l) {
                                    var e,
                                        n = Math.round(t.timeStamp);
                                    (e = n > 1e12 ? Date.now() - n : f.now() - n),
                                        (l = !0),
                                        c("timing", ["fi", n, { type: t.type, fid: e }]);
                                }
                            }
                            if (
                                !(
                                    "init" in NREUM &&
                                    "page_view_timing" in NREUM.init &&
                                    "enabled" in NREUM.init.page_view_timing &&
                                    NREUM.init.page_view_timing.enabled === !1
                                )
                            ) {
                                var a,
                                    s,
                                    c = t("handle"),
                                    f = t("loader"),
                                    u = NREUM.o.EV;
                                if (
                                    "PerformanceObserver" in window &&
                                    "function" == typeof window.PerformanceObserver
                                ) {
                                    (a = new PerformanceObserver(r)),
                                        (s = new PerformanceObserver(o));
                                    try {
                                        a.observe({ entryTypes: ["paint"] }),
                                            s.observe({ entryTypes: ["largest-contentful-paint"] });
                                    } catch (d) {}
                                }
                                if ("addEventListener" in document) {
                                    var l = !1,
                                        p = [
                                            "click",
                                            "keydown",
                                            "mousedown",
                                            "pointerdown",
                                            "touchstart",
                                        ];
                                    p.forEach(function (t) {
                                        document.addEventListener(t, i, !1);
                                    });
                                }
                            }
                        },
                        {},
                    ],
                    16: [
                        function (t, e, n) {
                            function r() {
                                function t() {
                                    return e ? 15 & e[n++] : (16 * Math.random()) | 0;
                                }
                                var e = null,
                                    n = 0,
                                    r = window.crypto || window.msCrypto;
                                r &&
                                r.getRandomValues &&
                                (e = r.getRandomValues(new Uint8Array(31)));
                                for (
                                    var o,
                                        i = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx",
                                        a = "",
                                        s = 0;
                                    s < i.length;
                                    s++
                                )
                                    (o = i[s]),
                                        "x" === o
                                            ? (a += t().toString(16))
                                            : "y" === o
                                            ? ((o = (3 & t()) | 8), (a += o.toString(16)))
                                            : (a += o);
                                return a;
                            }
                            function o() {
                                function t() {
                                    return e ? 15 & e[n++] : (16 * Math.random()) | 0;
                                }
                                var e = null,
                                    n = 0,
                                    r = window.crypto || window.msCrypto;
                                r &&
                                r.getRandomValues &&
                                Uint8Array &&
                                (e = r.getRandomValues(new Uint8Array(31)));
                                for (var o = [], i = 0; i < 16; i++) o.push(t().toString(16));
                                return o.join("");
                            }
                            e.exports = { generateUuid: r, generateCatId: o };
                        },
                        {},
                    ],
                    17: [
                        function (t, e, n) {
                            function r(t, e) {
                                if (!o) return !1;
                                if (t !== o) return !1;
                                if (!e) return !0;
                                if (!i) return !1;
                                for (
                                    var n = i.split("."), r = e.split("."), a = 0;
                                    a < r.length;
                                    a++
                                )
                                    if (r[a] !== n[a]) return !1;
                                return !0;
                            }
                            var o = null,
                                i = null,
                                a = /Version\/(\S+)\s+Safari/;
                            if (navigator.userAgent) {
                                var s = navigator.userAgent,
                                    c = s.match(a);
                                c &&
                                s.indexOf("Chrome") === -1 &&
                                s.indexOf("Chromium") === -1 &&
                                ((o = "Safari"), (i = c[1]));
                            }
                            e.exports = { agent: o, version: i, match: r };
                        },
                        {},
                    ],
                    18: [
                        function (t, e, n) {
                            function r(t, e) {
                                var n = [],
                                    r = "",
                                    i = 0;
                                for (r in t) o.call(t, r) && ((n[i] = e(r, t[r])), (i += 1));
                                return n;
                            }
                            var o = Object.prototype.hasOwnProperty;
                            e.exports = r;
                        },
                        {},
                    ],
                    19: [
                        function (t, e, n) {
                            function r(t, e, n) {
                                e || (e = 0),
                                "undefined" == typeof n && (n = t ? t.length : 0);
                                for (
                                    var r = -1, o = n - e || 0, i = Array(o < 0 ? 0 : o);
                                    ++r < o;

                                )
                                    i[r] = t[e + r];
                                return i;
                            }
                            e.exports = r;
                        },
                        {},
                    ],
                    20: [
                        function (t, e, n) {
                            e.exports = {
                                exists:
                                    "undefined" != typeof window.performance &&
                                    window.performance.timing &&
                                    "undefined" !=
                                    typeof window.performance.timing.navigationStart,
                            };
                        },
                        {},
                    ],
                    ee: [
                        function (t, e, n) {
                            function r() {}
                            function o(t) {
                                function e(t) {
                                    return t && t instanceof r ? t : t ? c(t, s, i) : i();
                                }
                                function n(n, r, o, i) {
                                    if (!l.aborted || i) {
                                        t && t(n, r, o);
                                        for (
                                            var a = e(o), s = m(n), c = s.length, f = 0;
                                            f < c;
                                            f++
                                        )
                                            s[f].apply(a, r);
                                        var d = u[y[n]];
                                        return d && d.push([x, n, r, a]), a;
                                    }
                                }
                                function p(t, e) {
                                    w[t] = m(t).concat(e);
                                }
                                function h(t, e) {
                                    var n = w[t];
                                    if (n)
                                        for (var r = 0; r < n.length; r++)
                                            n[r] === e && n.splice(r, 1);
                                }
                                function m(t) {
                                    return w[t] || [];
                                }
                                function v(t) {
                                    return (d[t] = d[t] || o(n));
                                }
                                function g(t, e) {
                                    f(t, function (t, n) {
                                        (e = e || "feature"), (y[n] = e), e in u || (u[e] = []);
                                    });
                                }
                                var w = {},
                                    y = {},
                                    x = {
                                        on: p,
                                        addEventListener: p,
                                        removeEventListener: h,
                                        emit: n,
                                        get: v,
                                        listeners: m,
                                        context: e,
                                        buffer: g,
                                        abort: a,
                                        aborted: !1,
                                    };
                                return x;
                            }
                            function i() {
                                return new r();
                            }
                            function a() {
                                (u.api || u.feature) &&
                                ((l.aborted = !0), (u = l.backlog = {}));
                            }
                            var s = "nr@context",
                                c = t("gos"),
                                f = t(18),
                                u = {},
                                d = {},
                                l = (e.exports = o());
                            l.backlog = u;
                        },
                        {},
                    ],
                    gos: [
                        function (t, e, n) {
                            function r(t, e, n) {
                                if (o.call(t, e)) return t[e];
                                var r = n();
                                if (Object.defineProperty && Object.keys)
                                    try {
                                        return (
                                            Object.defineProperty(t, e, {
                                                value: r,
                                                writable: !0,
                                                enumerable: !1,
                                            }),
                                                r
                                        );
                                    } catch (i) {}
                                return (t[e] = r), r;
                            }
                            var o = Object.prototype.hasOwnProperty;
                            e.exports = r;
                        },
                        {},
                    ],
                    handle: [
                        function (t, e, n) {
                            function r(t, e, n, r) {
                                o.buffer([t], r), o.emit(t, e, n);
                            }
                            var o = t("ee").get("handle");
                            (e.exports = r), (r.ee = o);
                        },
                        {},
                    ],
                    id: [
                        function (t, e, n) {
                            function r(t) {
                                var e = typeof t;
                                return !t || ("object" !== e && "function" !== e)
                                    ? -1
                                    : t === window
                                        ? 0
                                        : a(t, i, function () {
                                            return o++;
                                        });
                            }
                            var o = 1,
                                i = "nr@id",
                                a = t("gos");
                            e.exports = r;
                        },
                        {},
                    ],
                    loader: [
                        function (t, e, n) {
                            function r() {
                                if (!E++) {
                                    var t = (b.info = NREUM.info),
                                        e = p.getElementsByTagName("script")[0];
                                    if (
                                        (setTimeout(u.abort, 3e4),
                                            !(t && t.licenseKey && t.applicationID && e))
                                    )
                                        return u.abort();
                                    f(y, function (e, n) {
                                        t[e] || (t[e] = n);
                                    }),
                                        c("mark", ["onload", a() + b.offset], null, "api");
                                    var n = p.createElement("script");
                                    (n.src = "https://" + t.agent),
                                        e.parentNode.insertBefore(n, e);
                                }
                            }
                            function o() {
                                "complete" === p.readyState && i();
                            }
                            function i() {
                                c("mark", ["domContent", a() + b.offset], null, "api");
                            }
                            function a() {
                                return O.exists && performance.now
                                    ? Math.round(performance.now())
                                    : (s = Math.max(new Date().getTime(), s)) - b.offset;
                            }
                            var s = new Date().getTime(),
                                c = t("handle"),
                                f = t(18),
                                u = t("ee"),
                                d = t(17),
                                l = window,
                                p = l.document,
                                h = "addEventListener",
                                m = "attachEvent",
                                v = l.XMLHttpRequest,
                                g = v && v.prototype;
                            NREUM.o = {
                                ST: setTimeout,
                                SI: l.setImmediate,
                                CT: clearTimeout,
                                XHR: v,
                                REQ: l.Request,
                                EV: l.Event,
                                PR: l.Promise,
                                MO: l.MutationObserver,
                            };
                            var w = "" + location,
                                y = {
                                    beacon: "bam.nr-data.net",
                                    errorBeacon: "bam.nr-data.net",
                                    agent: "js-agent.newrelic.com/nr-1167.min.js",
                                },
                                x = v && g && g[h] && !/CriOS/.test(navigator.userAgent),
                                b = (e.exports = {
                                    offset: s,
                                    now: a,
                                    origin: w,
                                    features: {},
                                    xhrWrappable: x,
                                    userAgent: d,
                                });
                            t(12),
                                t(15),
                                p[h]
                                    ? (p[h]("DOMContentLoaded", i, !1), l[h]("load", r, !1))
                                    : (p[m]("onreadystatechange", o), l[m]("onload", r)),
                                c("mark", ["firstbyte", s], null, "api");
                            var E = 0,
                                O = t(20);
                        },
                        {},
                    ],
                    "wrap-function": [
                        function (t, e, n) {
                            function r(t) {
                                return !(t && t instanceof Function && t.apply && !t[a]);
                            }
                            var o = t("ee"),
                                i = t(19),
                                a = "nr@original",
                                s = Object.prototype.hasOwnProperty,
                                c = !1;
                            e.exports = function (t, e) {
                                function n(t, e, n, o) {
                                    function nrWrapper() {
                                        var r, a, s, c;
                                        try {
                                            (a = this),
                                                (r = i(arguments)),
                                                (s = "function" == typeof n ? n(r, a) : n || {});
                                        } catch (f) {
                                            l([f, "", [r, a, o], s]);
                                        }
                                        u(e + "start", [r, a, o], s);
                                        try {
                                            return (c = t.apply(a, r));
                                        } catch (d) {
                                            throw (u(e + "err", [r, a, d], s), d);
                                        } finally {
                                            u(e + "end", [r, a, c], s);
                                        }
                                    }
                                    return r(t)
                                        ? t
                                        : (e || (e = ""),
                                            (nrWrapper[a] = t),
                                            d(t, nrWrapper),
                                            nrWrapper);
                                }
                                function f(t, e, o, i) {
                                    o || (o = "");
                                    var a,
                                        s,
                                        c,
                                        f = "-" === o.charAt(0);
                                    for (c = 0; c < e.length; c++)
                                        (s = e[c]),
                                            (a = t[s]),
                                        r(a) || (t[s] = n(a, f ? s + o : o, i, s));
                                }
                                function u(n, r, o) {
                                    if (!c || e) {
                                        var i = c;
                                        c = !0;
                                        try {
                                            t.emit(n, r, o, e);
                                        } catch (a) {
                                            l([a, n, r, o]);
                                        }
                                        c = i;
                                    }
                                }
                                function d(t, e) {
                                    if (Object.defineProperty && Object.keys)
                                        try {
                                            var n = Object.keys(t);
                                            return (
                                                n.forEach(function (n) {
                                                    Object.defineProperty(e, n, {
                                                        get: function () {
                                                            return t[n];
                                                        },
                                                        set: function (e) {
                                                            return (t[n] = e), e;
                                                        },
                                                    });
                                                }),
                                                    e
                                            );
                                        } catch (r) {
                                            l([r]);
                                        }
                                    for (var o in t) s.call(t, o) && (e[o] = t[o]);
                                    return e;
                                }
                                function l(e) {
                                    try {
                                        t.emit("internal-error", e);
                                    } catch (n) {}
                                }
                                return t || (t = o), (n.inPlace = f), (n.flag = a), n;
                            };
                        },
                        {},
                    ],
                },
                {},
                ["loader", 2, 9]
            ));
    </script>
    <style type="text/css">
        body {
            color: #5b5b5b;
            background-color: #e8e8e8;
        }
        div.background-wrapper {
            border-color: #5b5b5b;
        }
        a,
        a:link,
        a:visited,
        h1,
        h2,
        h3 {
            color: #5b5b5b;
        }
    </style>
</head>

<body>
<div class="screen-width-indent">
    <div id="visitorSiteLogo">
        <img
            src="http://files.ctctcdn.com/a092c4ea501/d6aef104-f746-4d0e-bea2-af5117362d29.png"
            border="0"
        />
    </div>

    <div>
        <script
            src="/core/js/jquery/1.7.1/plugins/jquery.ux.infoPopup.js?version=20161010091324"
            language="javascript"
            id="javascript"
        ></script>

        <link
            href="/core/css/jquery/1.7.1/plugins/jquery.ux.infoPopup.css?version=20161010091324"
            media="all"
            rel="stylesheet"
            type="text/css"
        />

        <script>
            var $jq = jQuery;
        </script>

        <div id="optin" class="container">
            <h1 style="text-align: center;">Sign up to stay in touch!</h1>

            <form
                id="addForm"
                action="/manage/optin?v=001jnnzhBYoTji8-j13KJp_VCnlT-DgzDNPxFcxkq8myMbxKK-IyIoFLjYXdxq52kHD14r2rauEVPOV7u6qEXTMALObA2vFNP50kEpgPr7TTvw%3D"
                method="post"
                accept-charset="UTF-8"
            >
                <div class="form-background-wrapper clearfix">
                    <div class="colLeft">
                        <p>
                            Sign up to get interesting news and updates delivered to your
                            inbox.
                        </p>

                        <input
                            id="subscriberProfile.visitorPropertiesSize"
                            name="subscriberProfile.visitorPropertiesSize"
                            type="hidden"
                            value="2"
                        />

                        <div id="subProfile">
                            <fieldset>
                                <div class="prop-input clearfix" data-name="Email Address">
                                    <label for="inputProp0">
                                        <span class="required">*</span>

                                        Email Address
                                    </label>

                                    <input
                                        id="inputProp0"
                                        name="subscriberProfile.visitorProps[0].value"
                                        data-input="Email Address"
                                        type="text"
                                        value=""
                                        maxlength="50"
                                    />

                                    <input
                                        id="subscriberProfile.visitorProps0.id"
                                        name="subscriberProfile.visitorProps[0].id"
                                        type="hidden"
                                        value="80"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps0.name"
                                        name="subscriberProfile.visitorProps[0].name"
                                        type="hidden"
                                        value="Email Address"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps0.maxLen"
                                        name="subscriberProfile.visitorProps[0].maxLen"
                                        type="hidden"
                                        value="50"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps0.required"
                                        name="subscriberProfile.visitorProps[0].required"
                                        type="hidden"
                                        value="true"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps0.inputType"
                                        name="subscriberProfile.visitorProps[0].inputType"
                                        type="hidden"
                                        value="input"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps0.refDataKey"
                                        name="subscriberProfile.visitorProps[0].refDataKey"
                                        type="hidden"
                                        value=""
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps0.customPropID"
                                        name="subscriberProfile.visitorProps[0].customPropID"
                                        type="hidden"
                                        value=""
                                    />
                                </div>

                                <div class="prop-input clearfix" data-name="First Name">
                                    <label for="inputProp1">
                                        <span class="required">*</span>

                                        First Name
                                    </label>

                                    <input
                                        id="inputProp1"
                                        name="subscriberProfile.visitorProps[1].value"
                                        data-input="First Name"
                                        type="text"
                                        value=""
                                        maxlength="50"
                                    />

                                    <input
                                        id="subscriberProfile.visitorProps1.id"
                                        name="subscriberProfile.visitorProps[1].id"
                                        type="hidden"
                                        value="100"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps1.name"
                                        name="subscriberProfile.visitorProps[1].name"
                                        type="hidden"
                                        value="First Name"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps1.maxLen"
                                        name="subscriberProfile.visitorProps[1].maxLen"
                                        type="hidden"
                                        value="50"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps1.required"
                                        name="subscriberProfile.visitorProps[1].required"
                                        type="hidden"
                                        value="true"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps1.inputType"
                                        name="subscriberProfile.visitorProps[1].inputType"
                                        type="hidden"
                                        value="input"
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps1.refDataKey"
                                        name="subscriberProfile.visitorProps[1].refDataKey"
                                        type="hidden"
                                        value=""
                                    />
                                    <input
                                        id="subscriberProfile.visitorProps1.customPropID"
                                        name="subscriberProfile.visitorProps[1].customPropID"
                                        type="hidden"
                                        value=""
                                    />
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div id="colLeftContent" class="colLeft" style="display: none;">
                        <input
                            id="subscriberProfile.visitorParams"
                            name="subscriberProfile.visitorParams"
                            type="hidden"
                            value="001jnnzhBYoTji8-j13KJp_VCnlT-DgzDNPxFcxkq8myMbxKK-IyIoFLjYXdxq52kHD14r2rauEVPOV7u6qEXTMALObA2vFNP50kEpgPr7TTvw="
                        />
                        <input
                            id="subscriptions.interestCategoriesSize"
                            name="subscriptions.interestCategoriesSize"
                            type="hidden"
                            value="1"
                        />

                        <div id="intCats" style="display: none;">
                            <input
                                id="subscriptions.interestCategories0.id"
                                name="subscriptions.interestCategories[0].id"
                                type="hidden"
                                value="1206812549"
                            />
                            <input
                                id="subscriptions.interestCategories0.name"
                                name="subscriptions.interestCategories[0].name"
                                type="hidden"
                                value="General Interest"
                            />
                            <input
                                id="subscriptions.interestCategories0.selected1"
                                name="subscriptions.interestCategories[0].selected"
                                type="checkbox"
                                value="true"
                                checked="checked"
                            /><input
                                type="hidden"
                                name="_subscriptions.interestCategories[0].selected"
                                value="on"
                            />
                            <span></span>
                        </div>
                    </div>
                    <div class="colLeft">
                        <div id="displayCaptcha">
                            <input
                                id="captcha.url"
                                name="captcha.url"
                                type="hidden"
                                value="http://visitor.r20.constantcontact.com/manage/optin"
                            />
                            <input
                                id="captcha.remoteAddr"
                                name="captcha.remoteAddr"
                                type="hidden"
                                value="92.244.142.196"
                            />
                            <input
                                id="capResponse"
                                name="captcha.response"
                                type="hidden"
                                value=""
                            />
                            <input
                                id="captcha.key"
                                name="captcha.key"
                                type="hidden"
                                value="6LcB5AIAAAAAANx7J2ADgUFxwd_zllfY4DxX81c5"
                            />
                            <input
                                id="captcha.control"
                                name="captcha.control"
                                type="hidden"
                                value="001gRtjOnjW5WFzjvuBekDefkztscs4b10l"
                            />
                        </div>
                    </div>

                    <div
                        class="txt-mid foot-container update-profile-footer-container"
                    >
                        <div class="footer_copy update-profile-footer">
                            By submitting this form, you are consenting to receive
                            marketing emails from: Carolina Arms Group, 256 Raceway Drive
                            Suite 7, Mooresville , NC, 28117 United States,
                            http://www.carolinaarmsgroup.com. You can revoke your consent
                            to receive emails at any time by using the
                            <img
                                src="https://imgssl.constantcontact.com/letters/images/safe_unsubscribe_envelop.gif"
                                width="12"
                                height="9"
                                border="0"
                                alt="SafeUnsubscribe"
                                title="SafeUnsubscribe"
                            />
                            <strong>SafeUnsubscribe®</strong> link, found at the bottom of
                            every email.
                            <a
                                href="//www.constantcontact.com/legal/service-provider"
                                target="_blank"
                            >Emails are serviced by Constant Contact.</a
                            >
                        </div>
                    </div>

                    <div class="clear"></div>

                    <div class="visitorActions clearfix">
                        <input
                            name="_save"
                            type="submit"
                            id="update-profile-submit-btn"
                            class="btn btn-primary full"
                            value="Sign Up"
                        />
                    </div>
                </div>
            </form>
        </div>

        <script type="text/javascript">
            (function ($) {
                $("#emailFormat").hide();

                $("#colLeftContent").hide();
                $(".colRight").removeClass("colRight").addClass("colLeft");
            })(jQuery);

            var ea = {
                toggleForm: function () {
                    if ($jq("#updateEa:visible").length > 0) {
                        this.hideForm();
                    } else {
                        this.showForm();
                    }
                    $jq("#updateEa").slideToggle();
                },
                showForm: function () {
                    $jq(".visitorActions input[name=_save]").attr(
                        "disabled",
                        "disabled"
                    );
                    $jq("#updateEaSuccess").hide();
                },

                hideForm: function () {
                    $jq(".visitorActions input[name=_save]").removeAttr("disabled");
                    $jq("#eaUpdateErrors").hide();
                    $jq("#updateEaForm :text").val("");
                },

                update: function () {
                    $jq("#eaUpdateErrors").hide();

                    $jq.ajax({
                        type: "POST",
                        url: $jq("#updateEaForm").attr("action"),
                        dataType: "json",
                        data: $jq("#updateEaForm").serialize(),
                        success: function (data, textStatus) {
                            ea._handleUpdateSuccess(data);
                        },
                        error: function (request, textStatus, errorThrown) {
                            ea._handleUpdateError(errorThrown);
                        },
                        beforeSend: function (request) {
                            $jq("#eaUpdateWait").toggle();
                        },
                        complete: function (request, textStatus) {
                            $jq("#eaUpdateWait").toggle();
                        },
                    });
                },

                _handleUpdateSuccess: function (rsp) {
                    if (rsp.status == "failure") {
                        this._displayError(rsp.fieldErrors.emailAddr);
                        return;
                    }

                    $jq("#emailAddr").text(rsp.ea.emailAddr);
                    $jq("#updateEaForm input[name=oldEmailAddr]").val(
                        rsp.ea.emailAddr
                    );
                    $jq("#updateEaSuccess").show();
                    this.hideForm();
                },

                _handleUpdateError: function (error) {
                    this._displayError(
                        "We are unable to update your email address at this time"
                    );
                },

                _displayError: function (error) {
                    error = error.replace(/\.ea\.[\w]+$/, ""); // Strip off .ea.emailAddr
                    $jq("#eaUpdateErrors").text(error);
                    $jq("#eaUpdateErrors").fadeIn("slow");
                },
            };

            jQuery(document).ready(function () {
                var address = "Address";
                var update_profile = "Update Profile";
                var yes_unsubscribe = "Unsubscribe";
                var street_address = "Street";
                var state_province = "Select a state/province";
                var country_label = "Select a country";
                var zip_code = "Zip Code";
                var city = "City";

                var buttonText = {
                    update_profile: $jq("<div/>").html(update_profile).text(),
                    yes_unsubscribe: $jq("<div/>").html(yes_unsubscribe).text(),
                };
                var strings = {
                    address: $jq("<div/>").html(address).text(),
                    street_address: $jq("<div/>").html(street_address).text(),
                    state_province: $jq("<div/>").html(state_province).text(),
                    country: $jq("<div/>").html(country_label).text(),
                    zip_code: $jq("<div/>").html(zip_code).text(),
                    city: $jq("<div/>").html(city).text(),
                };
                var country = $jq('[data-name="Country"]');
                var street = $jq('[data-name="Street Address"]');
                var state_province = $jq('[data-name="State/Province"]');
                var address_label = $jq("<div />", {
                    class: "prop-input clearfix",
                }).append(
                    $jq("<label />", {
                        html: strings.address,
                    })
                );

                var first_address = $jq(
                    " [data-name='Street Address'], [data-name='City'], [data-name='Zip Code'], [data-name='Country'], [data-name='State/Province'] "
                ).first();

                if (first_address.attr("data-name") !== "Country") {
                    country.insertBefore(first_address);
                }

                $jq('input[data-input="Street Address"]').attr(
                    "placeholder",
                    strings.street_address
                );
                $jq('input[data-input="City"]').attr("placeholder", strings.city);
                $jq('input[data-input="Zip Code"]').attr(
                    "placeholder",
                    strings.zip_code
                );
                $jq('[data-name="Country"]')
                    .find("select > option:first-child")
                    .text(strings.country);
                $jq('[data-name="State/Province"]')
                    .find("select > option:first-child")
                    .text(strings.state_province);
                $jq(
                    'div[data-name="Street Address"],div[data-name="State/Province"],div[data-name="City"],div[data-name="Zip Code"]'
                ).each(function () {
                    $jq(this).find("label").hide();
                });
                label_location = country.length ? country : first_address;
                address_label.insertBefore(label_location);
                if (first_address.find("span.required").length) {
                    address_label
                        .find("label")
                        .prepend('<span class="required">*</span>');
                }

                $jq("#optoutAllRadio").click(function () {
                    $jq("#intCatSelection").hide();
                    $jq(".visitorActions .btn-primary").val(
                        buttonText.yes_unsubscribe
                    );
                    $jq("#emailFormat :input").attr("disabled", "disabled");
                    $jq("#subProfile :input").attr("disabled", "disabled");
                });
                $jq("#optoutSomeRadio").click(function () {
                    $jq("#intCatSelection").slideDown();
                    $jq(".visitorActions .btn-primary").val(
                        buttonText.update_profile
                    );
                    $jq("#emailFormat :input").removeAttr("disabled");
                    $jq("#subProfile :input").removeAttr("disabled");
                });

                if ($jq("#optoutAllRadio:checked").length > 0) {
                    $jq("#optoutAllRadio").trigger("click");
                }

                $jq("#updateEmailAddrLink a").click(function () {
                    ea.toggleForm();
                });

                $jq("#updateEa .btn-primary").click(function () {
                    ea.update();
                    return false;
                });

                $jq("#updateEa .btn-secondary").click(function () {
                    ea.hideForm();
                    return false;
                });

                var setInputType = (function getInputType() {
                        var $input_id = state_province.find("input[value=1300]"),
                            $select_input = state_province.find(
                                'select[id^="inputProp"]'
                            ),
                            $text_input = $jq("<input />", {
                                id: $select_input.attr("id"),
                                "data-input": "State/Province",
                                name: $select_input.attr("name"),
                                type: "text",
                                maxlength: "50",
                                placeholder: "State/Province",
                            }),
                            init = function init(country_code) {
                                var show_dropdown_input =
                                    country_code === "us" || country_code === "ca";

                                if (show_dropdown_input && $text_input.is(":visible")) {
                                    $input_id.val(1300);
                                    $text_input.replaceWith($select_input);
                                } else if (
                                    !show_dropdown_input &&
                                    $select_input.is(":visible")
                                ) {
                                    $input_id.val(1400);
                                    $select_input.replaceWith($text_input);
                                }
                            };

                        return init;
                    })(),
                    country_select = country.find(" > select");

                if (country_select.val()) {
                    setInputType(country_select.val());
                }

                country_select.on("change", function onCountryUpdate() {
                    setInputType($jq(this).val());
                });

                $jq('input[data-input="Birthday"]')
                    .attr("placeholder", "MM/DD")
                    .attr("maxlength", "5");
                $jq('input[data-input="Anniversary"]')
                    .attr("placeholder", "MM/DD/YYYY")
                    .attr("maxlength", "10");

                // HTML5 Feature of "placeholder" in text input fields does not yet work in IE
                // this code will mimic the behavior of the placeholder attribute
                // so that it works in IE in the same way as other browsers
                // before executing the code, we expand the jquery.support object to check
                // if the placeholder feature works in the browser, if it does not
                // then we implement the javascript functions

                // code concept from: http://www.cssnewbie.com/cross-browser-support-for-html5-placeholder-text-in-forms/
                //detect if browser supports placeholders
                jQuery.support.placeholder = false;
                var test = document.createElement("input");
                if ("placeholder" in test) jQuery.support.placeholder = true;

                if (!jQuery.support.placeholder) {
                    //placeholder text is not supported
                    jQuery("body").on(
                        {
                            focus: function () {
                                if (
                                    jQuery(this).attr("placeholder") != "" &&
                                    jQuery(this).val() == jQuery(this).attr("placeholder")
                                ) {
                                    jQuery(this).val("").removeClass("hasPlaceholder");
                                }
                            },
                            blur: function () {
                                if (
                                    jQuery(this).attr("placeholder") != "" &&
                                    (jQuery(this).val() == "" ||
                                        jQuery(this).val() == jQuery(this).attr("placeholder"))
                                ) {
                                    jQuery(this)
                                        .val(jQuery(this).attr("placeholder"))
                                        .addClass("hasPlaceholder");
                                }
                            },
                        },
                        "[placeholder]"
                    );
                    jQuery("form").submit(function () {
                        jQuery(this)
                            .find(".hasPlaceholder")
                            .each(function () {
                                jQuery(this).val("");
                            });
                    });
                    setInterval(function () {
                        jQuery("[placeholder]:not('.init-ph')")
                            .blur()
                            .addClass("init-ph");
                    }, 500);
                }
            });

            function onSubmit(response) {
                $jq("#capResponse").val(response);
            }
        </script>

        <!-- tilesLayout/layout.jsp -->

        <div class="txt-mid foot-container">
            <div class="footer_copy"></div>
        </div>
    </div>
</div>

<!-- Google Analytics -->

<script>
    (function (i, s, o, g, r, a, m) {
        i["GoogleAnalyticsObject"] = r;
        (i[r] =
            i[r] ||
            function () {
                (i[r].q = i[r].q || []).push(arguments);
            }),
            (i[r].l = 1 * new Date());
        (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m);
    })(
        window,
        document,
        "script",
        "//www.google-analytics.com/analytics.js",
        "ga"
    );

    ga("create", "UA-2821686-14", "auto");
    ga("send", "pageview", {
        page: "signup.form",
        title: "signup.form",
    });
</script>

<!-- End Google Analytics -->

<script type="text/javascript">
    window.NREUM || (NREUM = {});
    NREUM.info = {
        errorBeacon: "bam.nr-data.net",
        licenseKey: "5ab79a9e36",
        agent: "",
        beacon: "bam.nr-data.net",
        applicationTime: 200,
        applicationID: "1784721",
        transactionName:
            "Y1MHYRBUCBJZBRJfW1oZL2YyGhIIVAMVelVNWRBBTVkHGFcTEhheR0Y=",
        queueTime: 0,
    };
</script>
</body>
</html>
