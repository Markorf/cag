<?php

// shared function

function getUploadsDir() {
    $uploads = wp_upload_dir();
    return esc_url($uploads['baseurl']);
}

function getContentDir() {
    $content = content_url();
    return esc_url($content);
}